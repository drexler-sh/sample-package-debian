# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.1] - 2022-02-13
### Added
- Added `console` to the image information

## [0.1.0] - 2021-03-31
### Added
- Created the Drexler package for Debian Linux
- Added a set definition for Debian (`10-slim` and `10.9-slim`)
